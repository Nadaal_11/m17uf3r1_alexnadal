using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

public class SwitchVCam : MonoBehaviour
{
    public Animator playerAnimator;
    [SerializeField]
    private PlayerInput _playerInput;
    [SerializeField]
    private int _priorityBoostAmount = 10;
    [SerializeField]
    private Canvas _thirdPersonCanvas;
    [SerializeField]
    private Canvas _aimCanvas;

    private InputAction _aimAction;
    private InputManager _inputManager;
    private CinemachineVirtualCamera virtualCamera;
    private void Awake()
    {
        virtualCamera = GetComponent<CinemachineVirtualCamera>();
        _aimAction = _playerInput.actions["Aim"];
        _inputManager = InputManager.Instance;
    }
    private void OnEnable()
    {
        _aimAction.performed += _ => StartAim();
        _aimAction.canceled += _ => CancelAim();
    }
    private void OnDisable()
    {
        _aimAction.performed -= _ => StartAim();
        _aimAction.canceled -= _ => CancelAim();
    }
    private void StartAim()
    {
        virtualCamera.Priority += _priorityBoostAmount;
        playerAnimator.SetBool("IsAiming", true);
        _aimCanvas.enabled = true;
        _thirdPersonCanvas.enabled = false;
    }
    private void CancelAim()
    {
        virtualCamera.Priority -= _priorityBoostAmount;
        playerAnimator.SetBool("IsAiming", false);
        _aimCanvas.enabled = false;
        _thirdPersonCanvas.enabled = true;
    }
}
