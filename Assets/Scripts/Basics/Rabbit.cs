using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rabbit : MonoBehaviour
{
    public float velocidad = 5f;
    public float tiempoCambioDireccion = 2f;
    private Vector3 direccion;
    private float tiempoActual;
    private bool hayValla;

    private Animator animator;

    void Start()
    {
        tiempoActual = Time.time;
        CambiarDireccion();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        transform.Translate(direccion * velocidad * Time.deltaTime);

        if (Time.time - tiempoActual > tiempoCambioDireccion)
        {
            CambiarDireccion();
            tiempoActual = Time.time;
        }

        // Establecer la velocidad de la animaci�n en funci�n de la velocidad del conejo
        animator.SetFloat("Velocidad", velocidad);

        // Girar el conejo para enfrentar la direcci�n en la que se est� moviendo
        if (direccion != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(direccion);
            animator.SetFloat("Rotacion", transform.localEulerAngles.y);
        }

        // Verificar si hay una valla en frente del conejo
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 1f))
        {
            if (hit.collider.CompareTag("Valla"))
            {
                hayValla = true;
            }
            else
            {
                hayValla = false;
            }
        }
        else
        {
            hayValla = false;
        }
    }

    void CambiarDireccion()
    {
        direccion = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized;
    }
}