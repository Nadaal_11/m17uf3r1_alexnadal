using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    public float sensitivity = 1f; // Sensibilidad del movimiento de la c�mara
    public CinemachineVirtualCamera virtualCamera; // Referencia a la c�mara virtual de Cinemachine

    void Start()
    {
        
    }

    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X"); // Obtener la posici�n del rat�n en el eje X
        float mouseY = Input.GetAxis("Mouse Y"); // Obtener la posici�n del rat�n en el eje Y

        virtualCamera.transform.RotateAround(Vector3.zero, Vector3.up, mouseX * sensitivity); // Rotar la c�mara virtual en torno al centro de la escena en funci�n de la posici�n del rat�n en el eje X
        virtualCamera.transform.RotateAround(Vector3.zero, virtualCamera.transform.right, mouseY * sensitivity); // Rotar la c�mara virtual en torno al centro de la escena en funci�n de la posici�n del rat�n en el eje Y
    }
}