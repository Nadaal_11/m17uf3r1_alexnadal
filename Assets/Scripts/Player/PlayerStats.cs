using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    public Animator playerAnimator;
    public static PlayerStats playerStats;
    public GameObject player;
    public Slider healthSlider;

    public bool isHit = false;
    public float health;
    public float maxHealth;

    public void Awake()
    {
        if (playerStats != null)
        {
            Destroy(playerStats);
        }
        else playerStats = this;
        DontDestroyOnLoad(this.gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        SetHealthUI();
    }

    public void DealDamage(float damage)
    {
        health -= damage;
        isHit = true;
        // playerAnimator.SetTrigger("hit");
        player.GetComponent<PlayerController>().Hitted();
        CheckDeath();
        SetHealthUI();
    }
    public void HealCharacter(float heal)
    {
        health += heal;
        CheckOverheal();
        CheckDeath();
        SetHealthUI();
    }
    private void SetHealthUI()
    {
        healthSlider.value = CalculateHealthPersentage();
    }
    private void CheckOverheal()
    {
        if (health > maxHealth)
        {
            health = maxHealth;
        }
    }
    private void CheckDeath()
    {
        if (health < 0)
        {
            StartCoroutine(WaitToAnimation());
        }
    }
    float CalculateHealthPersentage()
    {
        isHit = false;
        return health / maxHealth;
    }
    IEnumerator WaitToAnimation()
    {
        playerAnimator.SetBool("IsDead", true);
        yield return new WaitForSeconds(3f);
        health = 0;
        Destroy(player);
        SceneManager.LoadScene(1);
    }
}