using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Variables de movimiento
    public float speed = 6.0f;
    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;
    public float gravity = 15f;


    public float jumpHeight = 1.0f;
    public float crouchSpeed = 2.0f;
    public float runSpeed = 10.0f;
    private Vector3 moveDirection = Vector3.zero;
    private CharacterController _player;
    private Transform mainCamera;

    // Variables de estado
    private bool isCrouching = false;
    private bool isRunning = false;

    private Vector3 moveDir;
    

    void Start()
    {
        _player = GetComponent<CharacterController>();
        mainCamera = Camera.main.transform;
    }

    void Update()
    {

        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        Vector3 playerInput = new Vector3(horizontal,0f, vertical).normalized;
        SetGravity();

        if (playerInput.magnitude >= 0.1f)
        {
            float targetAngle = Mathf.Atan2(playerInput.x, playerInput.z) * Mathf.Rad2Deg + mainCamera.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);
            moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
            transform.rotation = Quaternion.Euler(0f, angle, 0f);
            
            _player.Move(moveDir.normalized * speed * Time.deltaTime);
        }
   

        if (Input.GetButtonDown("Jump") && _player.isGrounded)
        {
            moveDirection.y = Mathf.Sqrt(2 * jumpHeight * Physics.gravity.magnitude);
        }
    }
    void SetGravity()
    {
        moveDir.y = -gravity * Time.deltaTime;
    }
}