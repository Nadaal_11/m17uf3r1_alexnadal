using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.EnhancedTouch;

public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Tots els de inputs
    /// </summary>
    private InputManager _inputManager;
    [SerializeField]
    private PlayerInput _playerInput;
    private InputAction _runAction;
    private InputAction _crouchAction;
    private InputAction _danceAction;
    private InputAction _moveAction;
    private InputAction _jumpAction;



    
    // Configuracio del moviment
    public float speed;
    public float walkSpeed = 5.0f; 
    public float runSpeed = 10.0f;
    public float jumpForce = 0.5f;
    public float gravity;
    private float moveY;

    public float rotationSpeed = 5f;

    private CharacterController controller;
    private Transform _mainCamera;
    public Animator playerAnimator;

    public float turnSmoothTime = 0.1f;
    float turnSmoothVelocity;

  
    private Vector3 moveDir;
    private Vector2 moveInput;
    private Vector3 playerMove;

    private bool crouch;
    private bool dance;
    private bool jump;
    private bool _hitted = false;

    void Awake()
    {
        controller = GetComponent<CharacterController>();
        _mainCamera = Camera.main.transform;
        _inputManager = InputManager.Instance;

        _moveAction = _playerInput.actions["Move"];
        _danceAction = _playerInput.actions["Dance"];
        _runAction = _playerInput.actions["Run"];
        _crouchAction = _playerInput.actions["Crouch"];
        _jumpAction = _playerInput.actions["Jump"];

        Cursor.lockState = CursorLockMode.Locked;
    }


    void Update()
    {
        SetGravity();
        Dance();
        if (!dance)
        {
            Jump();
            Movement();
            Crouch();
        }


    }
    void Movement()
    {
        moveInput = _playerInput.actions["Move"].ReadValue<Vector2>();
        playerMove = new Vector3(moveInput.x, 0f, moveInput.y).normalized;
        playerMove = playerMove.x * _mainCamera.right.normalized + playerMove.z * _mainCamera.forward.normalized;

        if (playerMove.magnitude >= 0.1f)
        {
            Run();
        }
        else
        {
            playerMove = Vector3.zero;
            playerAnimator.SetFloat("speed", 0f);
        }
        Quaternion rotation = Quaternion.Euler(0f, _mainCamera.eulerAngles.y, 0f);
        transform.rotation = Quaternion.Lerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
        controller.Move((playerMove.normalized * speed + Vector3.up * moveY) * Time.deltaTime);
    }
    void Run()
    {
        // Detectar si s'est� fent c�rrer al clicar el shift
        if (_playerInput.actions["Run"].IsPressed())
        {
            speed = runSpeed;
            playerAnimator.SetFloat("speed", runSpeed/runSpeed);
        }
        else
        {
            speed = walkSpeed;
            playerAnimator.SetFloat("speed", walkSpeed/runSpeed);
        }
    }
    void Crouch()
    {
        if (_playerInput.actions["Crouch"].IsPressed() && !jump)
        {
            crouch = true;
            playerAnimator.SetBool("IsCrouching", crouch);
            playerAnimator.SetFloat("SpeedCrouch", 0f);

            if (playerMove.magnitude >= 0.1f)
            {
                playerAnimator.SetFloat("SpeedCrouch", walkSpeed);
            }
        }
        else
        {
            crouch = false;
            playerAnimator.SetBool("IsCrouching", crouch);
        }
    }
    void Jump()
    {
        // Aplicar la gravetat
        if (controller.isGrounded && _playerInput.actions["Jump"].IsPressed())
        {
           
            moveY = jumpForce;
        }
    }
    private void Dance()
    {
        if (_playerInput.actions["Dance"].IsPressed() && !crouch)
        {
            dance = true;
            playerAnimator.SetBool("IsDancing", dance);
            StartCoroutine(PlayerDance(9f));
        }
    }
    void SetGravity()
    {
        if (!controller.isGrounded)
        {
            moveY += gravity * Time.deltaTime;
            playerAnimator.SetBool("IsJumping", true);
        }
        else playerAnimator.SetBool("IsJumping", false);
    }
    IEnumerator PlayerDance(float time)
    {
        yield return new WaitForSeconds(time);
        dance = false;
        playerAnimator.SetBool("IsDancing", dance);
    }
    public void Hitted()
    {
        _hitted = true;

    }

    public void EndHitted()
    {
        _hitted = false;
    }
}