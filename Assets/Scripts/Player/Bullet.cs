using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private GameObject _bulletDecal;
    public float speed = 10f;

    public int damage = 30;
    private float _timeToDestroy = 3f;
    private Rigidbody rb;
    Cinemachine.CinemachineImpulseSource source;

    public Vector3 target { get; set; }
    public bool hit { get; set; }

    private void OnEnable()
    {
        Destroy(gameObject, _timeToDestroy);
    }
     void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
        if (!hit && Vector3.Distance(transform.position, target) < .01f ) 
        {
            Destroy(gameObject);
        }
    }
    /* private void OnCollisionEnter(Collision other)
     {
         Enemy enemy = other.gameObject.GetComponent<Enemy>();
         ContactPoint contact = other.GetContact(0);
         Instantiate(_bulletDecal, contact.point + contact.normal * .0001f, Quaternion.LookRotation(contact.normal));
         if (enemy != null)
         {
             enemy.TakeDamage(damage);
         }
         Destroy(gameObject);
     }*/
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.name);
        Enemy enemy = collision.gameObject.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.TakeDamage(damage);
        }

        Destroy(gameObject);
    }

    void Start()
    {
        source = GetComponent<Cinemachine.CinemachineImpulseSource>();
        source.GenerateImpulse(Camera.main.transform.forward*0.5f);
    }

    void OnTriggerEnter(Collider hitInfo)
    {
        Debug.Log(hitInfo.gameObject.name);
        Enemy enemy = hitInfo.GetComponent<Enemy>(); 
        if (enemy != null)
        {
            enemy.TakeDamage(damage); 
        }

        Destroy(gameObject); 
    }
}
