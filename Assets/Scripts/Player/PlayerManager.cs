using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance; 
    public int maxHealth = 100; 

    private int currentHealth; 


    void Awake()
    {
        instance = this;
    }


    void Start()
    {
        currentHealth = maxHealth;
    }


    public void TakeDamage(int damage)
    {
        currentHealth -= damage;

      
        if (currentHealth <= 0)
        {
            Die();
        }
    }


    private void Die()
    {

        Debug.Log("El jugador ha muerto");
        SceneManager.LoadScene(1);
    }
}