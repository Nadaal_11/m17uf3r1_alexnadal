using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRepos : MonoBehaviour
{
    private void Update()
    {
        transform.position = transform.parent.position - 0.8f * Vector3.up;
        transform.rotation = transform.parent.rotation;
    }
}
