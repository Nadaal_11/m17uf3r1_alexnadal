using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour
{
    // Variables para el Spawner de Enemigos
    public GameObject enemyPrefab; 
    public float spawnRadius;
    public float spawnDelay; 
    public int maxEnemies; 
    public int totalEnemiesToSpawn;
    private int currentEnemies; 

    // Funci�n de Spawn
    IEnumerator SpawnEnemy()
    {
        while (true)
        {
            if (currentEnemies >= maxEnemies && totalEnemiesToSpawn > 0)
            {
                break;
            }

            Vector3 randomPosition = transform.position + Random.insideUnitSphere * spawnRadius;
            NavMeshHit hit;
            if (NavMesh.SamplePosition(randomPosition, out hit, spawnRadius, NavMesh.AllAreas))
            {
                Instantiate(enemyPrefab, hit.position, Quaternion.identity);
                currentEnemies++;
            }

            yield return new WaitForSeconds(spawnDelay);
        }
        /* while (true)
         {
             if (currentEnemies < maxEnemies && totalEnemiesToSpawn > 0)
             {
                 Vector3 spawnPosition = transform.position + Random.insideUnitSphere * spawnRadius; // Posici�n aleatoria dentro del radio del Spawner
                 Instantiate(enemyPrefab, spawnPosition, Quaternion.identity); // Creaci�n del Enemigo
                 currentEnemies++; // Incremento del n�mero de enemigos
                 totalEnemiesToSpawn--; // Decremento del total de enemigos a generar
             }

             if (totalEnemiesToSpawn <= 0 && currentEnemies <= 0)
             {
                 // Si ya no quedan enemigos por generar y no hay enemigos en la escena, detener la generaci�n
                 StopCoroutine(SpawnEnemy());
             }

             yield return new WaitForSeconds(spawnDelay); // Delay entre Spawnings
         }*/
    }


    public void EnemyDestroyed()
    {
        currentEnemies--;
    }

    void Start()
    {
        StartCoroutine(SpawnEnemy());
    }
}