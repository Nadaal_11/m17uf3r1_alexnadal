using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyStateMachine : MonoBehaviour
{
    private enum EnemyState
    {
        Patrol,
        Chasing,
        Attack,
        Death
    }

    private EnemyState currentState;
    private Transform player;
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    private float distanceToPlayer;
    private float attackRange = 2.5f;
    private float alertTimeElapsed = 1f;
    private int currentPatrolPointIndex = 0;
    private Vector3[] patrolPoints = new Vector3[7];
    private bool isDead = false;
    public Coroutine crr = null;
    public List<GameObject> ObjectPoints;

    private void Start()
    {
        var go = GameObject.Find("Points").transform;
        for (int i = 0; i < go.childCount; i++)
        {
            ObjectPoints.Add(go.GetChild(i).gameObject);
            patrolPoints[i] = ObjectPoints[i].transform.position;
        }
            
        currentState = EnemyState.Patrol;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        // Set patrol points
  
       /* patrolPoints[0] = new Vector3(-10f, 0f, 0f);
        patrolPoints[1] = new Vector3(-0.01f, 0f, 10f);
        patrolPoints[2] = new Vector3(10f, 0f, 0f);*/
    }

    private void Update()
    {
        distanceToPlayer = Vector3.Distance(transform.position, player.position);

        switch (currentState)
        {
            case EnemyState.Patrol:
                PatrolState();
                break;
            case EnemyState.Chasing:
                ChasingState();
                break;
            case EnemyState.Attack:
                AttackState();
                break;
            case EnemyState.Death:
                DeathState();
                break;
        }

        // Update animations
        animator.SetFloat("Speed", navMeshAgent.velocity.magnitude);
        animator.SetBool("IsChasing", currentState == EnemyState.Chasing);
        animator.SetBool("IsAttacking", currentState == EnemyState.Attack);
        animator.SetBool("IsDead", isDead);
    }

    private void PatrolState()
    {
        if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance && !navMeshAgent.pathPending)
        {
            currentPatrolPointIndex = (currentPatrolPointIndex + 1) % patrolPoints.Length;
            navMeshAgent.SetDestination(patrolPoints[currentPatrolPointIndex]);
            Debug.Log("ESTIC ANANT AL PUNT " + patrolPoints[currentPatrolPointIndex]);
        }

        if (distanceToPlayer < 10f && CanSeePlayer())
        {
            currentState = EnemyState.Chasing;
        }
    }

    private void ChasingState()
    {
        navMeshAgent.SetDestination(player.position);

        if (distanceToPlayer < attackRange)
        {
            currentState = EnemyState.Attack;
        }

        if (!CanSeePlayer())
        {
            currentState = EnemyState.Patrol;
            alertTimeElapsed = 0f;
        }
    }
    private void AttackState()
    {
        navMeshAgent.isStopped = true;
        if(crr == null)
        {
            crr = StartCoroutine(LookAtPlayer());
        }
    
        if (distanceToPlayer > attackRange)
        {
            navMeshAgent.isStopped = false;
            currentState = EnemyState.Chasing;
        }
    }
    private void DeathState()
    {
        navMeshAgent.isStopped = true;
        GetComponent<Collider>().enabled = false;
        navMeshAgent.enabled = false;
        isDead = true;
    }
    private bool CanSeePlayer()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, player.position - transform.position, out hit, Mathf.Infinity))
        {
            if (hit.transform == player)
            {
                return true;
            }
        }

        return false;
    }
    IEnumerator LookAtPlayer()
    {
        transform.LookAt(player);
        yield return new WaitForSeconds(2f);
        crr = null;
    }
}

