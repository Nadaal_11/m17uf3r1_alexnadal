using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    private enum EnemyState
    {
        Patrol,
        Chasing,
        Attack,
        Death
    }

    private EnemyState currentState;
    private Transform player;
    private NavMeshAgent navMeshAgent;
    private Animator animator;
    private float distanceToPlayer;
    private float attackRange = 1.5f;
    private float timeToReturnToPatrol = 5f;
    private float timeToAlert = 3f;
    private float alertTimeElapsed = 0f;
    private float timeToPatrol = 0f;
    private int currentPatrolPointIndex = 0;
    private Vector3[] patrolPoints = new Vector3[3];
    private bool isDead = false;

    private void Start()
    {
        currentState = EnemyState.Patrol;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();

        // Set patrol points
        patrolPoints[0] = new Vector3(-10f, 0f, 0f);
        patrolPoints[1] = new Vector3(0f, 0f, 10f);
        patrolPoints[2] = new Vector3(10f, 0f, 0f);
    }

    private void Update()
    {
        distanceToPlayer = Vector3.Distance(transform.position, player.position);

        switch (currentState)
        {
            case EnemyState.Patrol:
                PatrolState();
                break;
            case EnemyState.Chasing:
                ChasingState();
                break;
            case EnemyState.Attack:
                AttackState();
                break;
            case EnemyState.Death:
                DeathState();
                break;
        }

        // Update animations
        animator.SetFloat("Speed", navMeshAgent.velocity.magnitude);
        animator.SetBool("IsChasing", currentState == EnemyState.Chasing);
        animator.SetBool("IsAttacking", currentState == EnemyState.Attack);
        animator.SetBool("IsDead", isDead);
    }

    private void PatrolState()
    {
        // Move to next patrol point if we've reached the current one
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance && !navMeshAgent.pathPending)
        {
            currentPatrolPointIndex = (currentPatrolPointIndex + 1) % patrolPoints.Length;
            navMeshAgent.SetDestination(patrolPoints[currentPatrolPointIndex]);
        }

        // Check if player is in sight
        if (distanceToPlayer < 10f && CanSeePlayer())
        {
            currentState = EnemyState.Chasing;
        }
    }

    private void ChasingState()
    {
        // Follow the player
        navMeshAgent.SetDestination(player.position);

        // If close enough to attack, switch to attack state
        if (distanceToPlayer < attackRange)
        {
            currentState = EnemyState.Attack;
        }


        if (!CanSeePlayer())
        {
            currentState = EnemyState.Patrol;
            alertTimeElapsed = 0f;
        }
    }
    private void AttackState()
    {
        // Stop moving
        navMeshAgent.isStopped = true;

        // Look at the player
        transform.LookAt(player);

        // If player is out of range, switch to chasing state
        if (distanceToPlayer > attackRange)
        {
            navMeshAgent.isStopped = false;
            currentState = EnemyState.Chasing;
        }
    }
    private void DeathState()
    {
        // Stop moving
        navMeshAgent.isStopped = true;

        // Disable collider and navmeshagent to prevent further collisions
        GetComponent<Collider>().enabled = false;
        navMeshAgent.enabled = false;

        // Play death animation
        animator.SetTrigger("Die");

        // Set isDead to true to prevent further updates
        isDead = true;
    }
    private bool CanSeePlayer()
    {
        // Check if there is line of sight to the player
        RaycastHit hit;
        if (Physics.Raycast(transform.position, player.position - transform.position, out hit, Mathf.Infinity))
        {
            if (hit.transform == player)
            {
                return true;
            }
        }

        return false;
    }
}
