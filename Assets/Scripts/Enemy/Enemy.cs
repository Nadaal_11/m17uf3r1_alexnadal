using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth;
    private Animator _animator;
    private NavMeshAgent navMeshAgent;

    void Start()
    {
        currentHealth = maxHealth;
        navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }
    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0)
        {
            RoomController._instance.AddEnemieKilled();
            StartCoroutine(WaitforAnimation());
        }
    }

    void Die()
    {
        Destroy(gameObject); 
    }
    IEnumerator WaitforAnimation()
    {
        // Stop moving
        navMeshAgent.isStopped = true;

        // Disable collider and navmeshagent to prevent further collisions
        GetComponent<Collider>().enabled = false;
        GetComponent<EnemyStateMachine>().enabled = false;
        navMeshAgent.enabled = false;
        _animator.SetBool("IsDead",true);
        yield return new WaitForSeconds(2f);
        Die();
    }
}

