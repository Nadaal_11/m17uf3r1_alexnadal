using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SlotController : MonoBehaviour
{
    public ItemData currentItem;
    public abstract void OnChange();
    public abstract void OnAwake();
    public abstract void OnUpdate();
    public abstract void OnLateUpdate();
}