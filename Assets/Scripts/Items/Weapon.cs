using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class Weapon : SlotController
{
    [SerializeField]
    private PlayerInput _playerInput;
    private InputAction _shootAction;
    private InputAction _rechargeAction;
    [SerializeField]
    private Transform _barrelTransform;
    [SerializeField]
    private float _bulletHitMissDistance = 25f;
    [SerializeField]
    private Transform _bulletParent;

    private AudioSource _audioSource;
    public AudioClip gunAudio;
    public WeaponDataSO weaponData;
    private Transform _mainCamera;
    public Text textBullets;
    public Text textRecharge;

    private bool _canShoot = true;

    private float _timeSinceLastShot;
    public float range = 100f;


    public override void OnChange()
    {
        throw new System.NotImplementedException();
    }

    public override void OnAwake()
    {
        _mainCamera = Camera.main.transform;
        _audioSource = GetComponent<AudioSource>();
        _shootAction = _playerInput.actions["Shoot"];
        _rechargeAction = _playerInput.actions["Recharge"];
        _audioSource.clip = gunAudio;
    }

    public override void OnUpdate()
    {
        _timeSinceLastShot += Time.deltaTime;

        if (_shootAction.IsPressed())
        {

            if (_canShoot && weaponData._currentAmmo > 0)
            {
                Shoot();
            }
        }

        if (_rechargeAction.IsPressed())
        {
            Reload();
        }
    }

    public override void OnLateUpdate()
    {
        throw new System.NotImplementedException();
    }
    private void Shoot()
    {

        for (int i = 0; i < weaponData.NumberBulletsXShoot; i++)
        {

            RaycastHit hit;
            GameObject bullet = GameObject.Instantiate(weaponData.bulletPF, _barrelTransform.position, Quaternion.identity, _bulletParent);
            //Time.timeScale = 0;
            _audioSource.Play();
            Bullet bulletController = bullet.GetComponent<Bullet>();
            if (Physics.Raycast(_mainCamera.position, _mainCamera.forward, out hit, Mathf.Infinity))
            {
                Debug.Log(hit.collider.gameObject.name);
                bulletController.target = hit.point;
                bulletController.hit = true;
            }
            else
            {
                bulletController.target = _mainCamera.position + _mainCamera.forward * _bulletHitMissDistance;
                bulletController.hit = false;
            }

        }

        _canShoot = false;
        StartCoroutine(ShootCoolDown());
        weaponData._currentAmmo--;
        _timeSinceLastShot = 0f;
        textBullets.text = weaponData._currentAmmo.ToString() + " | " + weaponData._maxAmmo.ToString();
        if (weaponData._currentAmmo <= 0)
        {
            Reload();
        }
    }

    private void Reload()
    {
        textRecharge.text = "Recharging...";
        if (_timeSinceLastShot > weaponData.timeToRecharge && weaponData._currentAmmo < weaponData.MaxAmmoCarried)
        {
            weaponData._currentAmmo = weaponData._maxAmmoLoadder;
            weaponData._maxAmmo -= weaponData._maxAmmoLoadder;
            _timeSinceLastShot = 0;
            textRecharge.text = " ";
        }
    }
    private IEnumerator ShootCoolDown()
    {
        yield return new WaitForSeconds(0.2f);
        _canShoot = true;
    }
}
