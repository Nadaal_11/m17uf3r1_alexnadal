using ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;

public class TmobilitatController : MonoBehaviour
{
    private AudioSource _audio;
    public AudioClip audioClip;
    public Inventory inventory;
    public ItemData tmobilitat;
    public GameObject Panel;

    public void Awake()
    {
        _audio = GetComponent<AudioSource>();
        _audio.clip = audioClip;
    }
    private void OnTriggerStay(Collider collision)
    {
        if (collision.tag == "Player")
        {
            Panel.SetActive(true);
            if (Input.GetKey(KeyCode.E))
            {
                StartCoroutine(WaitTwoSeconds());
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        Panel.SetActive(false);
    }
    IEnumerator WaitTwoSeconds()
    {
        _audio?.Play();
        yield return new WaitForSeconds(0.4f);
        ItemController.itemController.AddItem(tmobilitat);
        ItemController.itemController.UpdateUIWeapons();
        ItemController.itemController.EquipTMobilitat(tmobilitat);
        Destroy(gameObject);
    }
}
