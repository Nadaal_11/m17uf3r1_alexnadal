using ScriptableObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class KeyChecker : MonoBehaviour
{
    public Inventory inventory;
    public Text message;
    public ExtraItems key;
    public GameObject Panel;
    private void OnTriggerStay(Collider collision)
    {
        if (collision.tag == "Player")
        {
            Panel.SetActive(true);
            for (int i = 0; i < inventory.slots.Length; i++)
            {
                if (inventory.slots[i] == key)
                {
                    message.text = "Press 'E'";
                    if (Input.GetKey(KeyCode.E))
                    {
                        SceneManager.LoadScene(3);
                    }
                }
            }
        }else Panel.SetActive(false);
    }
}
