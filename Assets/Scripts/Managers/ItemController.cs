using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ScriptableObjects;

public class ItemController : MonoBehaviour
{
    public static ItemController itemController;

    public Inventory playerInventory;
    public SlotController currentController;
    public WeaponDataSO pistol;
    public ItemData currentItem;
    private GameObject _item;
    public Image[] images;
    private int inventoryIndex = 0;
    public GameObject slot;
    public GameObject haloSlot;
    public GameObject tMobilitatSlot;
    public GameObject wingsSlot;

    // Start is called before the first frame update
    void Awake()
    {
        playerInventory.VaciarInventario();
        AddItem(pistol);
        UpdateUIWeapons();
        if (itemController != null && itemController != this)
        {
            Destroy(this);
        }
        else
        {
            itemController = this;
        }
    }
    private void Update()
    {
        ChangeCurrentWeapon();
        if (currentController != null)
            currentController.OnUpdate();
    }

    public void AddItem(ItemData itemtoAdd)
    {
        bool trobat = false;
        for (int i = 0; i < playerInventory.slots.Length; i++)
        {
            try
            {
                if (playerInventory.slots[i] == itemtoAdd && itemtoAdd.GetType() == typeof(WeaponDataSO))
                {
                    var weapon = (WeaponDataSO)playerInventory.slots[i];
                    weapon.isIniatilize();
                    trobat = true;
                    break;
                }
            }
            catch { }
        }
        if (!trobat)
        {
            if (itemtoAdd.GetType() == typeof(WeaponDataSO))
            {
                var arma = (WeaponDataSO)itemtoAdd;
                playerInventory.AddItem(arma);
                arma.isIniatilize();
            }
            else
            {
                playerInventory.AddItem(itemtoAdd);
            }
        }

    }
    public void UpdateUIWeapons()
    {
        for (int i = 0; i < playerInventory.slots.Length; i++)
        {
            if (playerInventory.slots[i] != null)
            {
                images[i].sprite = playerInventory.slots[i].DefaultItemSprite;
            }
        }
    }
    public void ChangeCurrentWeapon()
    {
        /*if (Input.GetKeyDown(KeyCode.C))
        {
            inventoryIndex++;
            CheckIndex();
            Debug.Log(inventoryIndex);
            currentItem = playerInventory.slots[inventoryIndex];
            CheckItemType();
           // gameObject.GetComponent<SpriteRenderer>().sprite = currentItem.DefaultItemSprite;
        }*/

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            inventoryIndex = 0;
            currentItem = playerInventory.slots[inventoryIndex];
            CheckItemType();
            EquipWeapon();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            inventoryIndex = 1;
            currentItem = playerInventory.slots[inventoryIndex];
            CheckItemType();
            EquipWeapon();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            inventoryIndex = 2;
            currentItem = playerInventory.slots[inventoryIndex];
            CheckItemType();
            EquipWeapon();
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            inventoryIndex = 3;
            currentItem = playerInventory.slots[inventoryIndex];
            CheckItemType();
            EquipWeapon();
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            inventoryIndex = 4;
            currentItem = playerInventory.slots[inventoryIndex];
            CheckItemType();
            EquipWeapon();
        }
    }
    public void EquipWeapon()
    {
        if (_item != null)
            Destroy(_item);
        _item = Instantiate(currentItem.prefabObject, slot.transform.position, slot.transform.rotation);
        _item.transform.parent = slot.transform.parent;
    }
    public void EquipHalo(ItemData halo)
    {
        var gameObject = new GameObject();
        gameObject = Instantiate(halo.prefabObject, haloSlot.transform.position, haloSlot.transform.rotation);
        gameObject.transform.parent = haloSlot.transform.parent;
    }
    public void EquipTMobilitat(ItemData tmobilitat)
    {
        var gameObject = new GameObject();
        gameObject = Instantiate(tmobilitat.prefabObject, tMobilitatSlot.transform.position, tMobilitatSlot.transform.rotation);
        gameObject.transform.parent = tMobilitatSlot.transform.parent;
    }
    public void EquipWings(ItemData wings)
    {
        var gameObject = new GameObject();
        gameObject = Instantiate(wings.prefabObject, wingsSlot.transform.position, wingsSlot.transform.rotation);
        gameObject.transform.parent = wingsSlot.transform.parent;
    }
    public void CheckItemType()
    {
        if (currentItem != null)
        {
            if (currentItem.GetType() == typeof(WeaponDataSO))
            {

                currentController = GetComponent<Weapon>();
                var asd = (Weapon)currentController;
                asd.weaponData = (WeaponDataSO)currentItem;
                asd.OnAwake();
            }
        }

    }
    private void CheckIndex()
    {
        if (inventoryIndex > 4)
        {
            inventoryIndex = 0;
        }
        else if (playerInventory.slots[inventoryIndex] == null)
        {
            if (inventoryIndex + 1 > 4)
            {
                inventoryIndex = 0;
            }
            else if (playerInventory.slots[inventoryIndex + 1] != null)
            {
                inventoryIndex += 1;
            }
            else
            {
                inventoryIndex = 0;
            }
        }
    }
}
