using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    PlayerControls _playerControls;
    private static InputManager _instance;
    public static InputManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else _instance = this;
        _playerControls = new PlayerControls();
    }

    private void OnDestroy()
    {
        _instance = null;
    }

    void OnEnable()
    {
        _playerControls.Player.Enable();
    }

    void OnDisable()
    {
        _playerControls.Disable();
    }
    public Vector2 GetPlayerMovement()
    {
        return _playerControls.Player.Move.ReadValue<Vector2>();
    }

    public bool GetPlayerRun()
    {
        return _playerControls.Player.Run.IsPressed();
    }

    public bool GetPlayerJump()
    {
        return _playerControls.Player.Jump.IsPressed();
    }

    public bool GetPlayerCrouch()
    {
        return _playerControls.Player.Crouch.IsPressed();
    }

    public bool GetPlayerDance()
    {
        return _playerControls.Player.Dance.IsPressed();
    }
    public bool GetPlayerAim()
    {
        return _playerControls.Player.Aim.IsPressed();
    }
    public bool GetPlayerShoot()
    {
        return _playerControls.Player.Shoot.IsPressed();
    }


}
