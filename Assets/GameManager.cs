using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager _instance;
    public int kills = 0;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Game Manager is NULL!");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        _instance = this;
    }
    private void Update()
    {
        AddKills();
    }
    public void AddKills()
    {
        kills = RoomController._instance.enemiesKilled;
    }
}
