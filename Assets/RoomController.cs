using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RoomController : MonoBehaviour
{
    public static RoomController _instance;
    public int enemiesKilled;
    public static RoomController Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Room Controller is NULL!");
            }
            return _instance;
        }
    }
    public void Awake()
    {
        _instance = this;
    }
    public void AddEnemieKilled()
    {
        enemiesKilled += 1;
        Debug.Log(enemiesKilled + " ENEMIES KILLED");
        CheckIfEnemiesAreDead();
    }
    public void CheckIfEnemiesAreDead()
    {
        if (enemiesKilled == 10)
        {
            SceneManager.LoadScene(4);
        }
    }
}
