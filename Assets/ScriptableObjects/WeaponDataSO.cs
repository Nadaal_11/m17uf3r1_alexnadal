using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New WeaponDataSO", menuName = "My ScriptableObjects/Create Weapon Data", order = 2)]
public class WeaponDataSO : ItemData
{
    public Sprite BulletSprite;

    public int _maxAmmo;
    public int MaxAmmoCarried
    {
        get
        {
            return _maxAmmo;
        }
    }

    public int _maxAmmoLoadder;

    public int chargers;

    public int _currentAmmo;

    public int timeToRecharge;

    public float bulletspeed;

    public GameObject bulletPF;

    public int NumberBulletsXShoot;



    [SerializeField]
    private float _damage;
    public float Damage { get; }

    public void ClearBullets()
    {
        _currentAmmo = 0;
        _maxAmmo = 0;
    }

    public void isIniatilize()
    {
        _maxAmmo = _maxAmmoLoadder * chargers;
        _currentAmmo = _maxAmmoLoadder;
    }
}