using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ScriptableObjects {
    [CreateAssetMenu(fileName = "New inventary", menuName = "Inventary")]
    public class Inventory : ScriptableObject
    {
        public ItemData[] slots = new ItemData[5];
        public ItemData item;
        public void VaciarInventario()
        {
            slots = new ItemData[5];
        }
        public void InventoryStart()
        {
            VaciarInventario();
            slots[0] = item;
        }
        public void AddItem(ItemData item)
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (slots[i] == null)
                {
                    slots[i] = item;
                    return;
                }
            }
        }
        public void DeleteItem(ItemData item)
        {
            for (int i = 0; i < slots.Length; i++)
            {
                if (slots[i] == item)
                {
                    slots[i] = null;
                    return;
                }
            }
        }
    } 
}